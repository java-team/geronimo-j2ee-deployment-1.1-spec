Source: geronimo-j2ee-deployment-1.1-spec
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Eugenio Cano-Manuel Mendoza <eugeniocanom@gmail.com>
Build-Depends:
 debhelper (>= 9),
 default-jdk,
 maven-debian-helper (>= 1.6.5)
Build-Depends-Indep:
 default-jdk-doc,
 libmaven-javadoc-plugin-java
Standards-Version: 3.9.4
Vcs-Git: git://anonscm.debian.org/pkg-java/geronimo-j2ee-deployment-1.1-spec.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-java/geronimo-j2ee-deployment-1.1-spec.git
Homepage: http://geronimo.apache.org/

Package: libgeronimo-j2ee-deployment-1.1-spec-java
Architecture: all
Depends:
 ${misc:Depends},
 ${maven:Depends}
Recommends: ${maven:OptionalDepends}
Suggests: libgeronimo-j2ee-deployment-1.1-spec-java-doc
Description: Geronimo API implementation of the J2EE deployment 1.1 spec
 The goal of the Geronimo project is to produce a server runtime framework that
 pulls together the best Open Source alternatives to create runtimes that meet
 the needs of developers and system administrators. Its most popular
 distribution is a fully certified Java EE 5 application server runtime.
 .
 This package provides the Geronimo API implementation of the J2EE deployment
 1.1 spec.

Package: libgeronimo-j2ee-deployment-1.1-spec-java-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${maven:DocDepends}
Recommends: ${maven:DocOptionalDepends}
Suggests: libgeronimo-j2ee-deployment-1.1-spec-java
Description: Documentation for J2EE Deployment 1.1
 The goal of the Geronimo project is to produce a server runtime framework that
 pulls together the best Open Source alternatives to create runtimes that meet
 the needs of developers and system administrators. Its most popular
 distribution is a fully certified Java EE 5 application server runtime.
 .
 This package contains the API documentation of
 libgeronimo-j2ee-deployment-1.1-spec-java.
