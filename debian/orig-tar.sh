#!/bin/sh -e

# $1 = version
TAR=../geronimo-j2ee-deployment-1.1-spec_$2.orig.tar.gz
DIR=libgeronimo-j2ee-deployment-1.1-spec-java-$2.orig

# clean up the upstream tarball
svn export http://svn.apache.org/repos/asf/geronimo/specs/tags/geronimo-j2ee-deployment_1.1_spec-$2/ $DIR
GZIP=--best tar -c -z -f $TAR $DIR
rm -rf $DIR
rm ../geronimo-j2ee-deployment-$2

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
